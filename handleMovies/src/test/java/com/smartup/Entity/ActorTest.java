package com.smartup.Entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class ActorTest {
	
	/** The actor. */
	//@Spy
	@InjectMocks
	Actor actor;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.actor = new Actor();
	}

	/**
	 * Constructor TEST.
	 */
	@Test
	public void ConstructorTEST() {
		Assert.assertNotNull(new Actor());
	}
	
	/**
	 * Test getname.
	 */
	@Test
	public void testGetname() {
		actor.setName("test");
		Assert.assertNotNull(actor.getName());
	}
	
	/**
	 * Test get sur name.
	 */
	@Test
	public void testGetSurName() {
		actor.setSurname("test");
		Assert.assertNotNull(actor.getSurname());
	}
	
	/**
	 * Test get create at.
	 */
	@Test
	public void testGetCreateAt() {
		actor.setCreateAt(new Date());
		Assert.assertNotNull(actor.getCreateAt());
	}
	
	/**
	 * Test get update at.
	 */
	@Test
	public void testGetUpdateAt() {
		actor.setUpdateAt(new Date());
		Assert.assertNotNull(actor.getUpdateAt());
	}
	
	/**
	 * Test get movies.
	 */
	@Test
	public void testGetMovies() {
		Set<Movie>movies = new HashSet<>();
		Movie movie = new Movie();
		movie.setTitle("test_title");
		movie.setYear(1991);
		movie.setGenre("test_genre");
		movie.setCreateAt(new Date());
		movies.add(movie);
		actor.setMovies(movies);
		Assert.assertNotNull(actor.getMovies());
	}
	

}
