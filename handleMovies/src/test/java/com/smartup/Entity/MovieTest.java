package com.smartup.Entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class MovieTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class MovieTest {
	
	/** The movie. */
	@InjectMocks
	Movie movie;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.movie = new Movie();
	}
	
	/**
	 * Constructor TEST.
	 */
	@Test
	public void ConstructorTEST() {
		Assert.assertNotNull(new Movie());
	}
	
	/**
	 * Test getname.
	 */
	@Test
	public void testGetname() {
		movie.setTitle("test");
		Assert.assertNotNull(movie.getTitle());
	}

	/**
	 * Test genre.
	 */
	@Test
	public void testGenre() {
		movie.setGenre("test");
		Assert.assertNotNull(movie.getGenre());
	}
	
	/**
	 * Test year.
	 */
	@Test
	public void testYear() {
		movie.setYear(1991);
		Assert.assertNotNull(movie.getYear());
	}
	
	/**
	 * Test get create at.
	 */
	@Test
	public void testGetCreateAt() {
		movie.setCreateAt(new Date());
		Assert.assertNotNull(movie.getCreateAt());
	}
	
	/**
	 * Test get update at.
	 */
	@Test
	public void testGetUpdateAt() {
		movie.setUpdateAt(new Date());
		Assert.assertNotNull(movie.getUpdateAt());
	}
	
	/**
	 * Gets the actors.
	 *
	 * @return the actors
	 */
	@Test
	public void getActors() {
		Set<Actor>actors = new HashSet<>();
		Actor actor = new Actor();
		actor.setName("test_name");
		actor.setSurname("test_surname");
		actors.add(actor);
		movie.setActors(actors);
		Assert.assertNotNull(movie.getActors());
	}

}
