package com.smartup.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Movie;
import com.smartup.Service.impl.MovieServiceImpl;
import com.smartup.controller.impl.MovieController;


/**
 * The Class MovieControllerTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class MovieControllerTest {

	@Mock
	private MovieServiceImpl movieService;
	
	@Mock
	private BindingResult result;
	
	@InjectMocks
	private MovieController movieController;
	
    
   
    @Test
    public void findAll() {
    	assertThat(movieController.index(), is(notNullValue()));
    }
    
    @Test
    public void testFindById() {
    	movieController.findById(1L);
    	verify(movieService).findById(1L);
    }

	@Test
    public void testSave() {
		movieController.save(new Movie(), result);
    }
    
    @Test
    public void testUpdate() {
    	movieController.update(new Movie(), result, 1L);
    }
    
    @Test
    public void testDelete(){
    	movieController.delete(any());
    }

}
