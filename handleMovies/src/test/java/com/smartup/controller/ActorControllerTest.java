package com.smartup.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Actor;
import com.smartup.Service.impl.ActorServiceImpl;
import com.smartup.controller.impl.ActorController;


@RunWith(MockitoJUnitRunner.class) // esto inicia los mock, no es necesario entonces poner el @Before
public class ActorControllerTest {
	
	@Mock
	private ActorServiceImpl actorService;
    
	@Mock
	private BindingResult result;
	
	@InjectMocks
	private ActorController actorController;
	
	
 
    @Test
    public void findAll() {
    	assertThat(actorController.index(), is(notNullValue()));
    }
    
    @Test
    public void testFindById() {
    	actorController.findById(1L);
    	verify(actorService).findById(1L);
    }

	@Test
    public void testSave() {
		actorController.save(new Actor(), result);
    }
    
	@Test
    public void testUpdate() {
    	actorController.update(new Actor(), result, 1L);
    }
    
    @Test
    public void testDelete(){
    	actorController.delete(any());
    }  
}
