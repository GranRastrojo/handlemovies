package com.smartup.exceptions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ServerExceptionTest {
	
	@InjectMocks
	ServerException businessException;

	@Test
	public void testBusinessExceptionString() {
		businessException = new ServerException("test");
	}

}
