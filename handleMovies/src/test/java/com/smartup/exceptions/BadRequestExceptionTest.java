package com.smartup.exceptions;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BadRequestExceptionTest {

	@InjectMocks
	BadRequestException businessException;

	@Test
	public void testBusinessExceptionString() {
		businessException = new BadRequestException("test");
	}

}
