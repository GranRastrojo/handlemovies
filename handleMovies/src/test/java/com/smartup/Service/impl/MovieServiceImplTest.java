package com.smartup.Service.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Movie;
import com.smartup.dao.IMovieDao;
import com.smartup.exceptions.BadRequestException;
import com.smartup.exceptions.ServerException;


@RunWith(MockitoJUnitRunner.class)
public class MovieServiceImplTest {
	
	@Mock
	private IMovieDao daoMock;
	
	@Mock
	private BindingResult result;
	
	@Rule
    public ExpectedException thrown= ExpectedException.none();
	
	@InjectMocks
	private MovieServiceImpl movieService;
	

    @Test
    public void testFindAll() {
    	assertThat(movieService.findAll(), is(notNullValue()));
    	verify(daoMock).findAll();
    }
	
	@Test
    public void testFindById() {
    	when(daoMock.findById(1L)).thenReturn(Optional.of(new Movie()));
    	assertThat(movieService.findById(1L), is(notNullValue()));
    	verify(daoMock).findById(1L);
    }
	
    @Test
    public void testSave() {
    	movieService.save(new Movie(),result);
    }
    
    @Test
    public void testSaveMovieExist() {
    	Movie movie = new Movie();
    	when(daoMock.findBytitle(any())).thenReturn(movie);
    	assertThat(movieService.save(movie,result), is(notNullValue()));
    }
    
    @SuppressWarnings("serial")
	@Test(expected = ServerException.class)
    public void testSaveException() {
    	Movie movie = new Movie();
    	doThrow(new ServerException("..."){}).when(this.daoMock).save((movie));
    	movieService.save(movie,result);
    }
    
    @Test
    public void testSaveBindingResult() {
    	Mockito.when(result.hasErrors()).thenReturn(true);
    	movieService.save(new Movie(),result);
    }
    
    @Test
    public void testUpdate() {
    	Movie movie = new Movie();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(movie));
    	assertThat(movieService.update(new Movie(),result,1L), is(notNullValue()));
    	movieService.update(new Movie(),result,1L);
    }
    
    @Test
    public void testUpdateBindingResult() {
    	Movie movie = new Movie();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(movie));
    	Mockito.when(result.hasErrors()).thenReturn(true);
    	movieService.update(new Movie(),result,1L);
    }
    
    @SuppressWarnings("serial")
	@Test(expected = BadRequestException.class)
    public void testUpdateException() {
    	Movie movie = new Movie();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(movie));
    	doThrow(new BadRequestException("..."){}).when(this.daoMock).save((movie));
    	movieService.update(new Movie(),result,1L);
    }
    
    @Test
    public void testDelete() {
    	Movie movie = new Movie();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(movie));
    	assertThat(movieService.delete(1L), is(notNullValue()));
    	movieService.delete(1L);
    }
    
    @SuppressWarnings("serial")
    @Test(expected = ServerException.class)
    public void testDeleteException(){
    	Movie movie = new Movie();
    	when(daoMock.findById(any())).thenReturn(Optional.of(movie));
    	doThrow(new ServerException("..."){}).when(this.daoMock).deleteById((any()));
    	movieService.delete(any());
    }

    @Test
    public void testFindByTitle() {
    	movieService.findBytitle("test");
    	verify(daoMock).findBytitle("test");
    }
    
}
