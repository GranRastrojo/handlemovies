package com.smartup.Service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

@RunWith(MockitoJUnitRunner.class)
public class ValidatingServiceTest {
	
	@Mock
	BindingResult result;
	
	@InjectMocks
	ValidatingService validatingService;
	
	@Test
	public void validateErrorsTest() {
		Assert.assertNotNull(validatingService.validateErrors(result));
		
	}



}
