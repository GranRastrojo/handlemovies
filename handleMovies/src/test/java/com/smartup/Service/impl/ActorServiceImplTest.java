package com.smartup.Service.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Actor;
import com.smartup.dao.IActorDao;
import com.smartup.exceptions.BadRequestException;
import com.smartup.exceptions.ServerException;


@RunWith(MockitoJUnitRunner.class)
public class ActorServiceImplTest {
	
	@Mock
	private IActorDao daoMock;
	
	@Mock
	private BindingResult result;
	
	@Rule
    public ExpectedException thrown= ExpectedException.none();
	
	@InjectMocks
	private ActorServiceImpl actorService;


    @Test
    public void testFindAll() {
    	assertThat(actorService.findAll(), is(notNullValue()));
    	verify(daoMock).findAll();
    }
	
	@Test
    public void testFindById() {
    	when(daoMock.findById(1L)).thenReturn(Optional.of(new Actor()));
    	assertThat(actorService.findById(1L), is(notNullValue()));
    	verify(daoMock).findById(1L);
    }
	
    @Test
    public void testSave() {
    	when(daoMock.save(any(Actor.class))).thenReturn(new Actor());
    	Actor actor = new Actor();
    	assertThat(actorService.save(actor,result), is(notNullValue()));
    	verify(daoMock).save(actor);
    }

    @Test
    public void testSaveActorExist() {
    	Actor actor = new Actor();
    	when(daoMock.findByNameAndSurname(any(),any())).thenReturn(actor);
    	assertThat(actorService.save(actor,result), is(notNullValue()));
    }
    
    @SuppressWarnings("serial")
	@Test(expected = ServerException.class)
    public void testSaveException() {
    	Actor actor = new Actor();
    	doThrow(new ServerException("..."){}).when(this.daoMock).save((actor));
    	actorService.save(actor,result);
    }
    
    @Test
    public void testSaveBindingResult() {
    	Actor actor = new Actor();
    	Mockito.when(result.hasErrors()).thenReturn(true);
    	assertThat(actorService.save(actor,result), is(notNullValue()));	
    }
    
    @Test
    public void testUpdate() {
    	Actor actor = new Actor();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(actor));
    	assertThat(actorService.update(new Actor(),result,1L), is(notNullValue()));
    }
    
    @SuppressWarnings("serial")
    @Test(expected = BadRequestException.class)
    public void testUpdateException() {
    	Actor actor= new Actor();
    	when(daoMock.findById(any())).thenReturn(Optional.of(actor));
    	doThrow(new BadRequestException("..."){}).when(this.daoMock).save((any()));
    	actorService.update(actor,result,1L);
    }
    
    @Test
    public void testUpdateBindingResult() {
    	Actor actor = new Actor();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(actor));
    	Mockito.when(result.hasErrors()).thenReturn(true);
    	assertThat(actorService.update(new Actor(),result,1L), is(notNullValue()));
    }
    
    @Test
    public void testDelete() {
    	Actor actor = new Actor();
    	when(daoMock.findById(1L)).thenReturn(Optional.of(actor));
    	assertThat(actorService.delete(1L), is(notNullValue()));
    	actorService.delete(1L);
    }
    
    @SuppressWarnings("serial")
    @Test(expected = ServerException.class)
    public void testDeleteException() {
    	Actor actor= new Actor();
    	when(daoMock.findById(any())).thenReturn(Optional.of(actor));
    	doThrow(new ServerException("..."){}).when(this.daoMock).deleteById((any()));
    	actorService.delete(any());
    }

    @Test
    public void testFindByNameAndSurname() {
    	actorService.findByNameAndSurname("test", "test");
    	verify(daoMock).findByNameAndSurname("test", "test");
    }

}
