package com.smartup.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartup.Entity.Movie;
import com.smartup.Service.IMovieService;
import com.smartup.controller.IMovieController;

@RestController
@RequestMapping("/api")
public class MovieController implements IMovieController{

	private IMovieService movieService;
	
	@Autowired
	public MovieController(IMovieService movieService) {
		this.movieService = movieService;
	}

	@GetMapping("/movies")
	public List<Movie> index() {
		return movieService.findAll();
	}

	@GetMapping("/movies/{id}")
	public ResponseEntity<?>findById(@PathVariable Long id) {
		Movie movie= movieService.findById(id);
		return new ResponseEntity<Movie>(movie,HttpStatus.OK);
	}
	
	@PostMapping("/movies")
	public ResponseEntity<?>save(@Valid @RequestBody Movie movie,BindingResult result){
		return movieService.save(movie,result);
	}
	
	@PutMapping("/movies/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Movie movie,BindingResult result,@PathVariable Long id){
		return movieService.update(movie,result,id);
	}

	@DeleteMapping("/movies/{id}")
	public ResponseEntity<?>delete(@PathVariable Long id) {
		return movieService.delete(id);
	}
}
