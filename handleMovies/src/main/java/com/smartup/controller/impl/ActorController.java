package com.smartup.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartup.Entity.Actor;
import com.smartup.Service.IActorService;
import com.smartup.controller.IActorController;

@RestController
@RequestMapping("/api")
public class ActorController implements IActorController{
	
	private IActorService actorService;
	
	@Autowired
	 public ActorController(IActorService actorService) {
		this.actorService = actorService;
	}

	@GetMapping("/actors")
	public List<Actor> index() {
		return actorService.findAll();
	}
	
	@GetMapping("/actors/{id}")
	public ResponseEntity<?>findById(@PathVariable Long id) {
		Actor actor = actorService.findById(id);
		return new ResponseEntity<Actor>(actor,HttpStatus.OK);
	}
	
	@PostMapping("/actors")
	public ResponseEntity<?>save(@Valid @RequestBody Actor actor,BindingResult result) {
		return actorService.save(actor,result);
	}
	
	@PutMapping("/actors/{id}")
	public ResponseEntity<?>update(@Valid @RequestBody Actor actor,BindingResult result,@PathVariable Long id){
		return actorService.update(actor, result, id);
	}
	
	@DeleteMapping("/actors/{id}")
	public ResponseEntity<?>delete(@PathVariable Long id){
		return actorService.delete(id); 	
	}
}
