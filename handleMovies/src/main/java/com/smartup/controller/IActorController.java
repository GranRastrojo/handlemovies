package com.smartup.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.smartup.Entity.Actor;

public interface IActorController {
	
	public List<Actor> index();
	public ResponseEntity<?>findById(@PathVariable Long id);
	public ResponseEntity<?>save(@Valid @RequestBody Actor actor,BindingResult result);
	public ResponseEntity<?>update(@Valid @RequestBody Actor actor,BindingResult result,@PathVariable Long id);
	public ResponseEntity<?>delete(@PathVariable Long id);

}
