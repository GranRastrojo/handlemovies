package com.smartup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class HandleMoviesApplication.
 */
@SpringBootApplication
public class HandleMoviesApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(HandleMoviesApplication.class, args);
	}

}
