package com.smartup.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table ( name = "movies")
public class Movie extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "it cant be empty")
	private String title;
	
	@NotEmpty(message = "it cant be empty")
	private String genre;
	
	@NotNull
	private Integer year;
	
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "actors_movies", joinColumns = {
        @JoinColumn(name = "movie_id")}, inverseJoinColumns = {
        @JoinColumn(name = "actor_id")})
    @JsonIgnoreProperties("movies")
    private Set<Actor> actors = new HashSet<>();
        
}
