package com.smartup.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Gets the update at.
 *
 * @return the update at
 */
@Getter

/**
 * Sets the update at.
 *
 * @param updateAt the new update at
 */
@Setter

/**
 * Instantiates a new base entity.
 */
@NoArgsConstructor

/**
 * Instantiates a new base entity.
 *
 * @param id the id
 * @param createAt the create at
 * @param updateAt the update at
 */
@AllArgsConstructor
@MappedSuperclass
public abstract class BaseEntity {
 
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
 
	/** The create at. */
	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	
	/** The update at. */
	@Column(name = "update_at")
	@Temporal(TemporalType.DATE)
	private Date updateAt;
	
	/**
	 * Pre persist.
	 */
	@PrePersist
	public void prePersist() {
		createAt=new Date();
	}
	
	/**
	 * Pre P update.
	 */
	@PreUpdate
	public void prePUpdate() {
		updateAt=new Date();
	}
  
}