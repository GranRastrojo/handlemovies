package com.smartup.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table ( name = "actors")
public class Actor extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "it cant be empty")
	@Size(max = 12, min =2)
	@Column(nullable = false)
	private String name;
	
	@NotEmpty(message = "it cant be empty")
	private String surname;

    @ManyToMany(mappedBy = "actors", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("actors")
    private Set<Movie> movies= new HashSet<>();

}
