package com.smartup.dao;

import org.springframework.data.repository.CrudRepository;

import com.smartup.Entity.Actor;

/**
 * The Interface IActorDao.
 */
public interface IActorDao extends CrudRepository<Actor, Long>{
	
	/**
	 * Find by name and surname.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @return the actor
	 */
	public Actor findByNameAndSurname(String name,String surname);

}
