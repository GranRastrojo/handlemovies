package com.smartup.dao;

import org.springframework.data.repository.CrudRepository;

import com.smartup.Entity.Movie;


/**
 * The Interface IMovieDao.
 */
public interface IMovieDao extends CrudRepository<Movie, Long>{
	
	/**
	 * Find bytitle.
	 *
	 * @param title the title
	 * @return the movie
	 */
	public Movie findBytitle(String title);

}
