package com.smartup.exceptions;

public class BadRequestException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public BadRequestException(String entityError) {
		super(entityError + ", cause by: " + new Throwable().getLocalizedMessage());
	}

}
