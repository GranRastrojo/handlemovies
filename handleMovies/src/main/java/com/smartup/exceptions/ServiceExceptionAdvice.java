package com.smartup.exceptions;

import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.smartup.dto.CustomErrorResponseDto;

@ControllerAdvice // control de excepciones globales
public class ServiceExceptionAdvice {
   
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<CustomErrorResponseDto> handleNotFound(RuntimeException ex, WebRequest request) {
        CustomErrorResponseDto errors = new CustomErrorResponseDto();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.NOT_FOUND.value());
        
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    	
    }
   
    @ExceptionHandler({ServerException.class,SQLException.class, NullPointerException.class})
    public ResponseEntity<CustomErrorResponseDto> handleServerError(RuntimeException ex, WebRequest request) {
        CustomErrorResponseDto errors = new CustomErrorResponseDto();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    	
    }
    
    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<Object> handleBadRequest(RuntimeException ex, WebRequest request) {
        CustomErrorResponseDto errors = new CustomErrorResponseDto();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.BAD_REQUEST.value());
        
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    	
    }
}