package com.smartup.exceptions;

public class NotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public NotFoundException(String entityError) {
		super(entityError + ", cause by: " + new Throwable().getLocalizedMessage());
	}
}