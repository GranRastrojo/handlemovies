package com.smartup.exceptions;

public class ServerException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public ServerException(String entityError) {
		super(entityError + ", cause by: " + new Throwable().getLocalizedMessage());
	}

}
