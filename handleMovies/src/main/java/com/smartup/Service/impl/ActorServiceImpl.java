package com.smartup.Service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Actor;
import com.smartup.Service.IActorService;
import com.smartup.dao.IActorDao;
import com.smartup.exceptions.BadRequestException;
import com.smartup.exceptions.NotFoundException;
import com.smartup.exceptions.ServerException;


@Service
public class ActorServiceImpl implements IActorService{

	private static final Logger LOG = LoggerFactory.getLogger(ActorServiceImpl.class);
	
	private IActorDao actorDao;
	
	@Autowired
	public ActorServiceImpl(IActorDao actorDao) {
		this.actorDao = actorDao;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Actor> findAll() {
		LOG.info("<<calling service>> findAll..");
		return (List<Actor>) actorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Actor findById(Long id){
		LOG.info("<<calling service actor>> find id : "+ id);
		return actorDao.findById(id).orElseThrow(()-> new NotFoundException("Not found id: "+id));
	}

	@Override
	@Transactional
	public ResponseEntity<?> save(Actor actor,BindingResult result){
		LOG.info("<<calling service>> save actor : "+ actor.getName());
		Map<String,Object> response = new HashMap<>();
		Actor actorExist = new Actor();
		if(result.hasErrors()) {
			response = new ValidatingService().validateErrors(result);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		actorExist = actorDao.findByNameAndSurname(actor.getName(), actor.getSurname());
		if(actorExist != null) {
			response.put("message","the actor already exist!");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		try {
			actorDao.save(actor);
		}catch(ServerException e ) {
			throw new ServerException("Error-- saving actor: "+actor.getName());
		}
		response.put("message","actor created!");
		response.put("actor",actor);
		return  new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@Override
	@Transactional
	public ResponseEntity<?> update(Actor actor,BindingResult result,Long id) {
		Map<String,Object> response = new HashMap<>();
		Actor actorActual = actorDao.findById(id).orElseThrow(()-> new NotFoundException("Actor not Found id: "+id));
		LOG.info("<<calling service>> update actual actor : "+ actorActual.getName());
		Actor actorNew = null;
		if(result.hasErrors()) {
			response = new ValidatingService().validateErrors(result);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		actorActual.setName(actor.getName());
		actorActual.setSurname(actor.getSurname());
		actorActual.setMovies(actor.getMovies());
		try {
			actorNew = actorDao.save(actorActual);
		}catch(BadRequestException e) {
			throw new BadRequestException("bad request: id : "+id);
		}
		response.put("message", "actor updated!");
		response.put("actor", actorNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}

	@Override
	@Transactional
	public ResponseEntity<?> delete(Long id)  {
		LOG.info("<<calling service>> delete id : "+ id);
		Map<String,Object> response = new HashMap<>();
		Actor actorToDetele = actorDao.findById(id).orElseThrow(()-> new NotFoundException("Actor not Found id: "+id));
		try {
		actorDao.deleteById(actorToDetele.getId());
		}catch(ServerException e) {
			throw new ServerException("Error-- removing actor id: "+id);
		}
		response.put("message","actor deleted!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK); 
	}

	@Override
	@Transactional(readOnly = true)
	public Actor findByNameAndSurname(String name,String surname) {
		return actorDao.findByNameAndSurname(name,surname);
	}
}
