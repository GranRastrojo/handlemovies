package com.smartup.Service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Movie;
import com.smartup.Service.IMovieService;
import com.smartup.dao.IMovieDao;
import com.smartup.exceptions.BadRequestException;
import com.smartup.exceptions.NotFoundException;
import com.smartup.exceptions.ServerException;


@Service
public class MovieServiceImpl implements IMovieService{
	
	private static final Logger LOG = LoggerFactory.getLogger(MovieServiceImpl.class);
	
	private IMovieDao movieDao;
	
	@Autowired
	public MovieServiceImpl(IMovieDao movieDao) {
		this.movieDao = movieDao;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Movie> findAll() {
		return (List<Movie>) movieDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Movie findById(Long id)  {
		LOG.info("<<calling service movie>> find id : "+ id);
	   return  movieDao.findById(id).orElseThrow(()-> new NotFoundException("Not found id: "+id));

	}

	@Override
	@Transactional
	public ResponseEntity<?> save(Movie movie,BindingResult result) {
		LOG.info("<<calling service>> save movie : "+ movie.getTitle());
		Map<String,Object>response = new HashMap<>();
		Movie movieExist = null;
		if(result.hasErrors()) {
			response = new ValidatingService().validateErrors(result);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		movieExist = movieDao.findBytitle(movie.getTitle());
		if(movieExist != null) {
			response.put("message","the movie already exist!");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		try {
			movieDao.save(movie);
		}catch(ServerException e ) {
			LOG.error("Error-- saving movie: " + movie.getTitle());
			throw new ServerException("Error-- saving movie: " + movie.getTitle());
		}
		response.put("message","movie created!");
		response.put("movie",movie);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@Override
	@Transactional
	public ResponseEntity<?> update(Movie movie, BindingResult result, Long id) {
		Map<String, Object> response = new HashMap<>();
		Movie movieActual = movieDao.findById(id).orElseThrow(() -> new NotFoundException("Movie not Found id: "+id));
		LOG.info("<<calling service>> update actual movie : " + movieActual.getTitle());
		Movie movieNew = null;
		if (result.hasErrors()) {
			response = new ValidatingService().validateErrors(result);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		movieActual.setTitle(movie.getTitle());
		movieActual.setGenre(movie.getGenre());
		movieActual.setActors(movie.getActors());
		try {
			movieNew = movieDao.save(movieActual);
		} catch (BadRequestException e) {
			throw new BadRequestException("bad request: id : " + id);
		}
		response.put("message", "movie updated!");
		response.put("movie", movieNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@Override
	@Transactional
	public ResponseEntity<?> delete(Long id) {
		Map<String,Object> response = new HashMap<>();
		Movie  movieToDelete = movieDao.findById(id).orElseThrow(() -> new NotFoundException("Movie not Found id: "+id));
		try {
			movieDao.deleteById(movieToDelete.getId());
		}catch(ServerException e) {
			LOG.error("Error-- removing movie id: " + id);
			throw new ServerException("Error-- removing movie id: "+id);
		}
		response.put("message","movie deleted!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK); 	
	}

	@Override
	@Transactional(readOnly = true)
	public Movie findBytitle(String title){
		LOG.info("info-- find by title "+title);
		return movieDao.findBytitle(title);

	}
}
