package com.smartup.Service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;

public class ValidatingService {

	public Map<String, Object> validateErrors(BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		List<String> errors = result.getFieldErrors().stream()
				.map(err -> "the field " + err.getField() + " " + err.getDefaultMessage()).collect(Collectors.toList());
		response.put("errors", errors);
		return response;
	}
}
