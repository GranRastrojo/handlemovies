package com.smartup.Service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Movie;



public interface IMovieService {
	
	public List<Movie>findAll();
	public Movie findById(Long id);
	public ResponseEntity<?> delete (Long id) ;
	public Movie findBytitle(String title) ;
	ResponseEntity<?> save(Movie movie, BindingResult result);
	ResponseEntity<?> update(Movie movie, BindingResult result, Long id);

}
