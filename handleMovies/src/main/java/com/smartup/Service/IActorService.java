package com.smartup.Service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.smartup.Entity.Actor;
import com.smartup.exceptions.BadRequestException;
import com.smartup.exceptions.NotFoundException;
import com.smartup.exceptions.ServerException;


public interface IActorService {
	
	public List<Actor>findAll();
	public Actor findById(Long id) throws NotFoundException;
	public ResponseEntity<?> save(Actor movie,BindingResult result);
	public ResponseEntity<?> delete (Long id) ;
	public Actor findByNameAndSurname(String name,String surname) ;
	ResponseEntity<?> update(Actor actor, BindingResult result, Long id) throws ServerException, BadRequestException;

}
